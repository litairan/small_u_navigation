import request from '@/utils/request'

// 获取站点列表
export function getSiteList() {
  return request({
    url: '/Index/siteList',
    method: 'get',
  })
}

// 获取站点列表
export function getRecommendArticle() {
  return request({
    url: '/Index/recommendArticle',
    method: 'get',
  })
}

// 获取工具列表
export function getToolList() {
  return request({
    url: '/Index/toolList',
    method: 'get',
  })
}

// 获取搜索推荐词
export function searchSuggest(keyword) {
  return request({
    url: '/Index/searchSuggest',
    method: 'get',
    params: {
      keyword,
    },
  })
}

// 提交反馈信息
export function sendFeedback(data) {
  return request({
    url: '/Email/sendEmail',
    method: 'post',
    data,
  })
}
